from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import yaml

config = yaml.safe_load(open("config.yml"))
usr=config['usr']
pw=config['pw']
ip=config['ip']
pto=config['pto']
bd=config['bd']
dialecto=config['dialecto']

motor = create_engine(f'{dialecto}://{usr}:{pw}@{ip}:{pto}/{bd}')
Session = sessionmaker(bind=motor)
session = Session()
Base = declarative_base()
