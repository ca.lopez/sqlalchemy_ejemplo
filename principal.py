import yaml
import logging
import db
from models import *
logging.basicConfig(level=logging.INFO, filename='project.log', filemode='w')

def Usuario_ins(usr_nombre,usr_correo,usr_edad=0,usr_activo=1):
    '''Insertar usuario en tabla usuario '''
    usr = Usuario(nombre=usr_nombre, correo=usr_correo,activo=usr_activo,edad=usr_edad)
    db.session.add(usr)
    db.session.commit()
    logging.info('Usuario registrado')

if __name__ == '__main__':
    db.Base.metadata.create_all(db.motor)
    Usuario_ins('Juan Perez','juan.perez@servidor.com',20)
    registros = db.session.query(Usuario).get(1)
