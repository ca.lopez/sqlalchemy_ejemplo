import db
from sqlalchemy import Column, Integer, String, Float, Text, Boolean, ForeignKey

class Usuario(db.Base):
    __tablename__='usuario'
    id=Column(Integer, primary_key=True)
    nombre=Column('nombre', Text)
    correo=Column('correo', Text)
    activo=Column('activo', Boolean)
    edad=Column('edad', Integer)

class Comentario(db.Base):
    __tablename__='comentario'
    id=Column(Integer, primary_key=True)
    mensaje=Column('descripcion', String(20))
    id_u = Column(Integer, ForeignKey('usuario.id'))
